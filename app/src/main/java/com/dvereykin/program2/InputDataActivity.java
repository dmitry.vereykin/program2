package com.dvereykin.program2;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class InputDataActivity extends AppCompatActivity implements View.OnClickListener, Runnable {

    Button cmdOK;
    Button cmdClear;
    Button cmdRead;

    EditText editName;
    EditText editAddress;
    EditText editCity;
    EditText editState;
    private EditText editZipCode;

    String[] data = new String[5];
    String message = "";
    boolean isSave = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_data);

        cmdOK = (Button) findViewById(R.id.ok_button);
        cmdClear = (Button) findViewById(R.id.clear_button);
        cmdRead = (Button) findViewById(R.id.read_button);

        editName = (EditText) findViewById(R.id.editNameFld);
        editAddress = (EditText) findViewById(R.id.editAddressFld);
        editCity = (EditText) findViewById(R.id.editCityFld);
        editState = (EditText) findViewById(R.id.editStateFld);
        editZipCode = (EditText) findViewById(R.id.editZipCodeFld);

        cmdOK.setOnClickListener(this);
        cmdClear.setOnClickListener(this);
        cmdRead.setOnClickListener(this);
    }

    public void onClick(View v) {
        if (v.getId() == cmdOK.getId()) {

            data[0] = editName.getText().toString();
            data[1] = editAddress.getText().toString();
            data[2] = editCity.getText().toString();
            data[3] = editState.getText().toString();
            data[4] = editZipCode.getText().toString();

            if (data[0].equals("") || data[1].equals("") || data[2].equals("")
                    || data[3].equals("") || data[4].equals("")) {
                createDialog("Alert!", "One or more of the fields are empty");
            } else {

                message += data[0] + ", " + data[1] + ", " + data[2] + ", " + data[3]
                        + ", " + data[4];
                createDialog("Success! Data was saved.", message);

                try {
                    save();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        if (v.getId() == cmdRead.getId()) {

            for (int i = 0; i < 5; i++) {
                data[i] = "";
            }

            try {
                read();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            editName.setText(data[0]);
            editAddress.setText(data[1]);
            editCity.setText(data[2]);
            editState.setText(data[3]);
            editZipCode.setText(data[4]);
        }

        if (v.getId() == cmdClear.getId()) {
            clear();
        }
    }

    public void save() throws InterruptedException {
        isSave = true;
        Thread threadToSave = new Thread(this);
        threadToSave.start();
        threadToSave.join();
    }

    public void read() throws InterruptedException {
        isSave = false;
        Thread threadToRead = new Thread(this);
        threadToRead.start();
        threadToRead.join();
    }

    public void createDialog(String title, String data) {
        AlertDialog alertDialog = new AlertDialog.Builder(InputDataActivity.this).create();

        alertDialog.setTitle(title);
        alertDialog.setMessage(data);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int e) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();

    }

    public void clear() {
        editName.setText("");
        editAddress.setText("");
        editState.setText("");
        editCity.setText("");
        editZipCode.setText("");
        message = "";
        Toast.makeText(getApplicationContext(), "Cleared!", Toast.LENGTH_SHORT).show();
    }

    public void readData() {
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader
                                                        (openFileInput("addressfile")));
            for (int i = 0; i < 5; i++) {
                data[i] = br.readLine();
            }

            br.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void saveData() {
        try {
            OutputStreamWriter writer = new OutputStreamWriter
                            (openFileOutput("addressfile", Context.MODE_PRIVATE));

            for (int i = 0; i < 5; i++) {
                writer.append(data[i]);
                writer.append("\r\n");
            }

            writer.flush();
            writer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void run() {
        if (isSave)
            saveData();
        else
            readData();

    }

}
